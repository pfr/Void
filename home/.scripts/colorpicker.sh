#!/bin/sh

color=$(xcolor)

if [ -n "$color" ]; then
        temp=$(mktemp -t --suffix ".png")
        convert -size 100x100 xc:$color $temp
        echo $color | xsel -ib
        notify-send -i $temp "Colorpicker" "$color"
        #printf "IMG:$temp\t$color\n" >$XNOTIFY_FIFO
fi
