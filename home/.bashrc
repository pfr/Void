# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Make prompts cool again
user_color=$'\e[94;1m'
prompt_char=%
if (( EUID == 0 )) ; then
    user_color=$'\e[31;1m'
    prompt_char='#'
fi
PS1='\[$user_color\][\u]\[\e[0m\] \[\e[90;1m\]\w\[\e[0m\] \[$user_color\]$prompt_char\[\e[0m\] '

eval "$(zoxide init bash)"

. $HOME/.bash_aliases
