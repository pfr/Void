#!/bin/sh
#
# Dependencies: dmenu, feh, notify-send, image-magick, gimp, mediainfo, xclip

while read file
do
        case "$1" in

    # Set image as wallpaper
    "w")
        feh --bg-scale --no-fehbg "$file" &
        printf "\tWall set\t$file" >$XNOTIFY_FIFO &
        ;;

    # Copy file to another location
    "c")
		[ -z "$destdir" ] &&
            destdir="$(sed 's/\s+#.*$//; s/^\s*#//; /^\s*$/d' ${XDG_CONFIG_HOME:-$HOME/.config}/shell/bm-dirs |
            awk '{print $2}' |
            dmenu -fn 'CodeNewRomanNerdFont:style=Normal:pixelsize=14:antialias=true' \
					-nb '#2d2d2d' \
					-nf '#c0c5ce' \
					-sb '#5fb3b3' \
					-sf '#000000' \
                    -l 20 \
                    -i -p "Copy file(s) to where?" |
            sed "s|~|$HOME|g")"
		[ ! -d "$destdir" ] && notify-send -t 5000 -a "$destdir" "Not a directory, cancelled" && exit
		cp "$file" "$destdir" && notify-send -t 5000 -a "Copied To" "$destdir" &
		;;

	# Move file to another location
    "m")
		[ -z "$destdir" ] &&
            destdir="$(sed 's/\s+#.*$//; s/^\s*#//; /^\s*$/d' ${XDG_CONFIG_HOME:-$HOME/.config}/shell/bm-dirs |
            awk '{print $2}' |
            dmenu -fn 'CodeNewRomanNerdFont:style=Normal:pixelsize=14:antialias=true' \
                    -nb '#2d2d2d' \
                    -nf '#c0c5ce' \
                    -sb '#5fb3b3' \
                    -sf '#000000' \
                    -l 20 \
                    -i -p "Move file(s) to where?" |
            sed "s|~|$HOME|g")"
		[ ! -d "$destdir" ] && notify-send -t 5000 -a "$destdir" "Not a directory, cancelled" && exit
		mv "$file" "$destdir" && notify-send -t 5000 -a "Moved To" "$destdir" &
		;;

	# Rotate/flop image
	"r")
		convert -rotate 90 "$file" "$file" ;;
	"R")
		convert -rotate -90 "$file" "$file" ;;
	"f")
		convert -flop "$file" "$file" ;;

	# Copy file location
	"y")
		echo -n "$file" | tr -d '\n' | xclip -sel clip &&
            notify-send -t 5000 -a "File Location" "$(xclip -o -sel clip)"
		;;

    # Delete selected images, confirm using dmenu
    "d")
		[ "$(printf "No\\nYes" |
            dmenu -fn 'CodeNewRomanNerdFont:style=Normal:pixelsize=14:antialias=true' \
                    -nb '#2d2d2d' \
                    -nf '#c0c5ce' \
                    -sb '#5fb3b3' \
                    -sf '#000000' \
                    -i -p "Really delete $file?")" = "Yes" ] &&
            rm "$file" && notify-send -t 5000 -a "File Deleted" "Bye Bye"
        ;;

    # Open image in gimp
	"g")
        if ! command -v gimp 2>&1 >/dev/null
        then
            notify-send -t 5000 -a "Gimp" "Command not found"
            exit 1
        else
            gimp "$file"
        fi
        ;;

	# Print mediainfo to notification
	"i")
        notify-send -t 15000 -a "Media Info" "$(mediainfo "$file")" ;;

	# Upload image & copy url to clipboard
    "u")
        pb 0x0 "$file" |
		xclip -sel clip && notify-send -t 5000 -a "Image Uploaded" "$(xclip -o -sel clip)"
		;;

		esac
done
