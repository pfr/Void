#---------------------------------------------------------------
#                            Aliases
#---------------------------------------------------------------

# xbps
alias fpkg="fuzzypkg"
alias xupdate="doas xbps-install -Suv"
alias xinstall="doas xbps-install -S"
alias xsearch="xbps-query -R"
alias xremove="doas xbps-remove -RoO"
alias bootstrup="cd ~/xbpsrc/ && ./xbps-src bootstrap-update"
alias xupsrc="cd ~/xbpsrc/ && ./xbps-src update-sys"
alias zap="cd ~/xbpsrc/ && ./xbps-src zap"
alias binstrap="cd ~/xbpsrc/ && ./xbps-src binary-bootsrtap"

# random hlpful aliases
alias ..='cd ..'
alias mnt='doas mount -t msdos /dev/sda1 /mnt && cd /mnt'
alias umnt='cd && doas umount -t msdos /mnt'
alias ls='exa --icons --group-directories-first'
alias lc='exa --icons --group-directories-first && echo "(`ls | wc -l`)"'
alias ll='exa -l --icons --group-directories-first && echo "(`ls | wc -l`)"'
alias la='exa -al --icons --group-directories-first && echo "(`ls -a | wc -l`)"'
alias c="clear"
alias reload="source ~/.bashrc && clear && ufetch"
alias listen="mpv --no-video"
alias wttr="curl -s http://wttr.in/Melbourne | awk 'NR==2,NR==7'"
alias wttr2="curl v2.wttr.in/Melbourne | awk 'NR==39,NR==43'"
alias moon="~/.scripts/./moon.sh"
alias sdf="ssh pfr@tty.sdf.org"
alias anonradio="mpv --no-video http://anonradio.net:8000/anonradio"
alias fp="fzf --preview 'bat --style numbers,changes --color=always --theme=base16 --line-range=:500 {}'"
alias v="vim"
alias please='doas $(fc -ln -1)'
alias reboot="doas reboot"
alias goodbye="doas shutdown -h now"
alias et="sh ~/.scripts/empty-trash.sh"
alias h='fc -l -n -r 1 | sed -Ee "s/^[[:blank:]]+//" | uniq | fzf | tr -d \\n | xclip -selection c'
alias bat="bat --wrap character --theme=base16"
alias pat="bat -p --wrap character --theme=base16"
alias nat="bat -n --wrap character --theme=base16"
alias hn="hackernews_tui"
alias passgen="base64 /dev/urandom | head -c 30"
alias pia="doas startvpn"
alias gemi="amfora"
alias icat="kitty +kitten icat"

# nothing to see here
alias fap="cd ~/.fapdl/pron && nsxiv-thumb && cd"
alias fapdl=" cd ~/.fapdl/pron && yt-dlp -ciw -o '%(title)s.%(ext)s' --batch-file='~/.fapdl/batch-file.txt'"

# ytfzf
alias ytp="ytfzf -t"
alias ytd="cd ~/Videos/youtube && ytfzf -td"
alias ytpm="ytfzf -tm"
alias ytdm="cd ~/Music && ytfzf -tmd"

# kb-manager
alias kbl="kb list"
#alias kbe="kb edit"
alias kba="kb add"
#alias kbv="kb view"
#alias kbd="kb delete --id"
alias kbg="kb grep"
alias kbt="kb list --tags"

# git
alias status="git status"
alias add="git add ."
alias commit="git commit -m"
alias push="git push"
alias pull="git pull"

#---------------------------------------------------------------
#                           Functions
#---------------------------------------------------------------

# shfm cd on exit
# !use 'command cat' to avoid conflicts with bat(1)
s () {
    shfm "$@"
    cd "$(command cat "${XDG_CACHE_HOME:=${HOME}/.cache}/shfm/exit")"
}

# KNOWLEDGE BASE: ~/.kb
# kb-view
kbv() {
    cd $HOME/.kb ; \
    fzf --preview 'bat {}' --bind 'enter:become(bat {})' ; \
    cd
}
#kb-edit
kbe() {
    cd $HOME/.kb ; \
    fzf --preview 'bat {}' --bind 'enter:become(vim {})' ; \
    cd
}
#kb-del
kbd() {
    cd $HOME/.kb ; \
    rm $(fzf --preview 'bat {}') ; \
    cd
}
#kb-new
kbn() {
    cd /$HOME/.kb ; \
    vim ; \
    cd
}

# spawn swiv to preview wallpapers folder
walls() {
    swiv -tpbrq \
    -g 978x550 \
    -B '#1b1b1b' \
    -C '#e6e0d3' \
    $HOME/Pictures/walls
}

# spawn swiv to preview screenshots folder
dumps() {
    swiv -tpbrq \
    -g 978x550 \
    -B '#1b1b1b' \
    -C '#e6e0d3' \
    $HOME/Pictures/dumps
}

# mkdir and cd into it
md () { mkdir -p $1 && cd $1; }

# search & install packages using fzf (similar to fuzzypkg)
xs () {
    xpkg -a |
        fzf -m --preview 'xq {1}' \
            --preview-window=right:66%:wrap | \
        xargs -ro xi
}

# Swap 2 filenames around, if they exist
# usage: swap <file1> <file2>
swap () {
    local TMPFILE=tmp.$$
    mv "$1" $TMPFILE
    mv "$2" "$1"
    mv $TMPFILE "$2"
}

# Creates an archive (*.tar.gz) from given directory.
mktar() { tar cvzf "${1%%/}.tar.gz"  "${1%%/}/"; }

# Make your directories and files access rights sane.
sanitize() { chmod -R u=rwX,g=rX,o= "$@" ;}

# ARCHIVE EXTRACTION
# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1    ;;
      *.tar.gz)    tar xzf $1    ;;
      *.tar.xz)    tar xf $1     ;;
      *.tar)       tar xf $1     ;;
      *.tar.zst)   uzstd $1      ;;
      *.bz2)       bunzip2 $1    ;;
      *.rar)       unrar x $1    ;;
      *.gz)        gunzip $1     ;;
      *.tbz2)      tar xjf $1    ;;
      *.tgz)       tar xzf $1    ;;
      *.zip)       unzip $1      ;;
      *.Z)         uncompress $1 ;;
      *.7z)        7z x $1       ;;
      *.deb)       ar x $1       ;;
      *)      echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}
