" ~/.vimrc

" The below chunk will install vim.plug if not already installed
" Once installed you can comment this chunk out to reduce load time

"-----------------~-~-----------~-------------~-----------------~--~-----------
"if empty(glob('~/.vim/autoload/plug.vim'))
"    silent !curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs
"        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
"    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
"endif
"
"" Run PlugInstall if there are missing plugins
"autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
"    \| PlugInstall --sync | source $MYVIMRC
"\| endif
"----------~-----------~--------------~--~------------~---------~-~------------

" vim-plug
call plug#begin('~/.vim/plugged')
Plug 'mhinz/vim-startify'           " Neat start page with bookmarks etc
Plug 'ryanoasis/vim-devicons'       " Pretty icons for the startpage
Plug 'Raimondi/delimitMate'         " Autoclose Delimiters
call plug#end()

" Usage
" :PlugUpgrade    - upgrade vim.plug to the latest version
" :PlugInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PlugSearch foo - searches for foo; append `!` to refresh local cache
" :PlugClean      - removal of unused plugins; append `!` to auto-approve removal

" General settings
filetype plugin on
syntax on
colors garbage-oracle
set t_Co=256
set nocompatible
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set smarttab
set complete-=i
set backspace=indent,eol,start
set history=50
set ruler
set scrolloff=1
set sidescrolloff=5
set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
set wildmenu
set cursorline
set showmatch
set number
set relativenumber

" Toggle paste mode
set pastetoggle=<F3>

" Dont automatically add comments on new lines
autocmd BufNewFile,BufRead * setlocal formatoptions-=ro

" Color options
highlight LineNr ctermfg=DarkGrey
highlight LineNr ctermbg=NONE
highlight CursorLine cterm=NONE
highlight CursorLine ctermbg=Black
highlight CursorLineNr cterm=NONE
highlight CursorLineNr ctermbg=NONE
highlight CursorLineNr ctermfg=Red
highlight ColorColumn ctermbg=Black

" Status line
" To get a list of available colors run
" :so $VIMRUNTIME/syntax/hitest.vim
set laststatus=2			    "show statusbar
set statusline=				    "left side
set statusline+=%#LineNr#       "color
set statusline+=\ %y			"filetype
set statusline+=\ %r			"readonly flag
set statusline+=%#LineNr#       "colour
set statusline+=\ %F			"full path to file
set statusline+=%=			    "right side
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}    "utf-8
set statusline+=\ [%{&fileformat}\]    "[unix]
set statusline+=\ %#LineNr#     "color
set statusline+=\ %l/%L			"line numbers
set statusline+=\ [%c]			"column number

" Remove trailing white spaces
autocmd BufWritePre * %s/\s\+$//e

" Configure startify plugin
let g:startify_custom_header = [
\ '                                 ',
\ '      ⢀⣀ ⣰⡀ ⢀⣀ ⡀⣀ ⣰⡀ ⣀⡀ ⢀⣀ ⢀⡀ ⢀⡀ ',
\ '      ⠭⠕ ⠘⠤ ⠣⠼ ⠏  ⠘⠤ ⡧⠜ ⠣⠼ ⣑⡺ ⠣⠭ ',
\ ]


let g:startify_lists = [
\ { 'type': 'files',    'header': ['    last modified:'] },
\ { 'type': 'bookmarks', 'header': ['    bookmarks:'] },
\ { 'type': 'dir',  'header': ['    files in folder '. getcwd()]},
\ ]

let g:startify_bookmarks = [ {'v': '~/.vimrc'}, {'z': '~/.zshrc'}, {'a': '~/.zsh_aliases'}, {'x': '~/.xinitrc'}, {'A': '~/.config/alacritty/alacritty.toml'}, {'o': '~/.config/openbox/rc.xml'}, {'m': '~/.config/openbox/menu.xml'}, {'P': '~/.profile'}, {'X': '~/.Xresources'}, {'p': '~/.config/picom/picom.conf'}]
