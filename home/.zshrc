# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:/usr/local/bin:$HOME/.local/bin:$HOME/.cargo/bin:$PATH

# Path to Android platform tools
export PATH="/usr/local/bin/platform-tools:$PATH"

# Use hyphen-insensitive completion.
HYPHEN_INSENSITIVE="true"

# Automatically update without prompting.
DISABLE_UPDATE_PROMPT="true"

export UPDATE_ZSH_DAYS=13

# Enable command auto-correction.
ENABLE_CORRECTION="false"

# Display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Stamp shown in the history command output.
HIST_STAMPS="dd/mm/yyyy"

# required libs
source $HOME/.config/zsh/lib/completion.zsh
source $HOME/.config/zsh/lib/correction.zsh
source $HOME/.config/zsh/lib/git.zsh
source $HOME/.config/zsh/lib/history.zsh
source $HOME/.config/zsh/lib/prompt_info_functions.zsh
source $HOME/.config/zsh/lib/theme-and-appearance.zsh

# Theme
source $HOME/.config/zsh/themes/alanpeabody.zsh-theme

# Plugins
source $HOME/.config/zsh/plugins/colored-man-pages/colored-man-pages.plugin.zsh
source $HOME/.config/zsh/plugins/colorize/colorize.plugin.zsh
source $HOME/.config/zsh/plugins/extract/extract.plugin.zsh
source $HOME/.config/zsh/plugins/fancy-ctrl-z/fancy-ctrl-z.plugin.zsh
source $HOME/.config/zsh/plugins/fzf/fzf.plugin.zsh
source $HOME/.config/zsh/plugins/rust/rust.plugin.zsh
source $HOME/.config/zsh/plugins/zsh-interactive-cd/zsh-interactive-cd.plugin.zsh

# You may need to manually set your language environment
export LANG=en_AU.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='nextvi'
else
  export EDITOR='vim'
fi

# Compilation flags
export ARCHFLAGS="-arch x86_64"

# OpenAI token
export OPENAI_TOKEN=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

# Zoxide
eval "$(zoxide init zsh)"

# File opener
export SHFM_OPENER="$HOME/.open.sh"
export FFF_OPENER="$HOME/.open.sh"

# Use fd with fzf
export FZF_DEFAULT_COMMAND='fd --type file --follow --hidden --color=always'
export FZF_DEFAULT_OPTS='--height 100% --layout=reverse --border --ansi --preview-window=80%'

# Bitwarden menu flags
export RBW_MENU_COMMAND="dmenu -fn 'Sarasa Term CL Nerd Font-12' -nb '#181818' -nf '#ffd7af' -sb '#7f1e31' -sf '#ffd7af'"

# Command Not Found insulter
if [ -f /etc/bash.command-not-found ]; then
    . /etc/bash.command-not-found
fi

export HISTIGNORESPACE=1

source $HOME/.zsh_aliases
